# calculator_REST_Mqtt

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/anton.mail.kbr/calculator_rest_mqtt.git
git branch -M main
git push -uf origin main
```


## Name
REST API Calculator with Mqtt interface

## Description
This is a simple REST API for a calculator that supports basic arithmetic operations. Currwntly  addition, subtraction, multiplication, and division. At the moment only the additional function is implemented. 

##Installation
To get started, clone the repository to your local machine:

bash
```
git clone https://github.com/yourusername/calculator-rest-api.git
```

Next, navigate to the project directory and install the dependencies:

bash
```
cd calculator-rest-api
npm install
```


## Usage
To start the server, run the following command:

```
npm start
```
This will start the server on port 3000 by default. You can then send requests to the API using a tool such as Postman or cURL.

## Endpoints
The following endpoints are available:

POST /add
Adds two numbers and returns the result.

Request body:

```
Copy code
{
  "number1": 5,
  "number2": 3
}
```

Response body:

```
{
  "result": 8
}

```

## MQTT Topics
add

This topic receives messages containing two integers a and b as the payload and returns their sum as a response message.

Request
Topic: add
Payload: String containing two integers separated by a comma.

```
10,20
```

Response
Topic: result
Payload: String representing the sum of the two integers.

```
30
```

## Testing
To run the tests, use the following command:

bash
```
npm test
```

This will run both the unit tests and the integration tests.

## Functions
add(a: number, b: number): number

This function receives two numbers a and b and returns their sum.

Parameters
a: A number representing the first operand.
b: A number representing the second operand.
Return Value
A number representing the sum of the two operands.


handleAddRequest(req: express.Request, res: express.Response): void

This function handles the HTTP POST request to the /api/add endpoint. It parses the request body for the two integers a and b, calls the add function with those operands, and returns the result as a JSON response.

Parameters
req: An express.Request object representing the HTTP request.
res: An express.Response object representing the HTTP response.
Return Value
None



handleMqttAddMessage(topic: string, message: Buffer): void

This function handles the incoming MQTT message on the add topic. It parses the message for the two integers a and b, calls the add function with those operands, and publishes the result as a message on the result topic.

Parameters
topic: A string representing the MQTT topic on which the message was received.
message: A Buffer object representing the payload of the MQTT message.
Return Value
None
## Roadmap
Version 1.0.0
Add basic functionality for adding two numbers
Implement REST API to receive requests and respond with results
Implement MQTT integration for handling requests and responses

Version 1.1.0
Implement subtraction functionality
Update REST API to handle subtract requests
Update MQTT integration to handle subtract requests

Version 1.2.0
Implement multiplication functionality
Update REST API to handle multiply requests
Update MQTT integration to handle multiply requests

Version 1.3.0
Implement division functionality
Update REST API to handle divide requests
Update MQTT integration to handle divide requests

Version 1.4.0
Add support for handling multiple requests at the same time
Improve error handling and logging for better troubleshooting
Implement automated testing for all functionality
Improve documentation and examples
