const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());

//creates a new Express server and listens on port 3000.
// When a POST request is received on the /add endpoint, it parses the request body and adds the two integers.
// The result is then sent back as a JSON response. 
//The server also logs a message to the console when it starts listening.

app.post('/api/add', (req, res) => {
  const a = parseInt(req.body.a);
  const b = parseInt(req.body.b);

  if (isNaN(a) || isNaN(b)) {
    return res.status(400).json({ error: 'Invalid input: a and b must be numbers' });
  }

  const result = a + b;
  res.json({ result });
});

app.listen(8080, () => {
  console.log('REST API server listening on port 8080');
});

module.exports = app;