const mqtt = require('mqtt');

const client = mqtt.connect('mqtt://localhost');

//This code creates an MQTT client and connects to the local MQTT broker. 
//Once connected, it subscribes to the calculator/add topic. 
//When a message is received on this topic, it parses the payload, 
//adds the two integers and publishes the result on the calculator/add/result topic.

client.on('connect', () => {
  console.log('MQTT client connected');

  client.subscribe('calculator/add', (err) => {
    if (err) {
      console.error('Error subscribing to MQTT topic', err);
      return;
    }
    console.log('MQTT client subscribed to calculator/add topic');
  });
});

client.on('message', (topic, message) => {
  if (topic === 'calculator/add') {
    const payload = JSON.parse(message.toString());
    const a = parseInt(payload.a);
    const b = parseInt(payload.b);
    const result = a + b;
    client.publish('calculator/add/result', JSON.stringify({ result }));
  }
});
