const request = require('supertest');
const app = require('../src/rest');

describe('REST API server', () => {
  
  test('should add two numbers correctly', async () => {
    const res = await request(app).post('/api/add').send({ a: 2, b: 3 });
    expect(res.statusCode).toEqual(200);
    expect(res.body.result).toEqual(5);
  });

  test('should handle invalid input', async () => {
    const res = await request(app).post('/api/add').send({ a: 'not a number', b: 3 });
    expect(res.statusCode).toEqual(400);
  });
});
